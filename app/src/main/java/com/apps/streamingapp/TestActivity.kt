package com.apps.streamingapp

import android.content.DialogInterface
import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.SurfaceView
import kotlinx.android.synthetic.main.activity_test.*
import android.media.AudioManager
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog


class TestActivity : AppCompatActivity(), SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener{
    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        showToast("error")
        return true
    }

    private fun showToast(info: String) {
        Toast.makeText(this,info, Toast.LENGTH_SHORT).show()
    }

    override fun onCompletion(mp: MediaPlayer?) {
        mp?.let {
            mediaPlayer -> mediaPlayer.stop()
            mediaPlayer.release()
        }
        showAlertDialog()
    }

    private fun showAlertDialog() {
        val  alertDialog = AlertDialog.Builder(this@TestActivity)
        alertDialog.setTitle("Video finished.")
        alertDialog.setMessage("Do you want to watch again?")
        alertDialog.setPositiveButton("yes") {
                dialog, which -> dialog.dismiss()
                startAgain()
        }
        alertDialog.setNegativeButton("no") {
            dialog, which -> dialog.dismiss()
            quitWatching()
        }
        alertDialog.setCancelable(true)
        alertDialog.create().show()
    }

    private fun quitWatching() {
        mediaPlayer.let {
            mediaPlayer -> mediaPlayer.stop()
            goToHome()
        }
    }

    private fun goToHome() {
        val intent = Intent(this@TestActivity, MainActivity::class.java)
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(intent)
        finish()
    }

    private fun startAgain() {
        mediaPlayer.let {
            mediaPlayer -> mediaPlayer.seekTo(0)
            mediaPlayer.start()
        }
    }

    private lateinit var mediaPlayer: MediaPlayer
    var vidAddress = "https://fs157.gounlimited.to/tea5vhprp32qzxfffpyippt55gwxdrot574bj4dzq5xmbwujmluifg44tvuq/La.casa.de.papel.A.K.A.Money.Heist.S01E12.DUAL-AUDIO.SPA-ENG.720p.10bit.WEBRip.2CH.x265.HEVC-PSA.mkv.mp4"

    private val handler: Handler = Handler()

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        try {
            mediaPlayer = MediaPlayer()
            mediaPlayer.setDisplay(surface_view.holder)
            mediaPlayer.setDataSource(vidAddress)
            mediaPlayer.prepare()
            mediaPlayer.setOnPreparedListener(this)
            mediaPlayer.setScreenOnWhilePlaying(true)
            mediaPlayer.setOnCompletionListener(this)
            mediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mediaPlayer.start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_test)
        hideNavigationBar()


        intent?.let {
            val url= intent.getStringExtra("url") as String
            if (url.isNotEmpty())
                vidAddress = url
        }

        surface_view.holder.addCallback(this)
    }

    private fun hideNavigationBar() {
        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            // Note that system bars will only be "visible" if none of the
            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                // TODO: The system bars are visible. Make any desired
                // adjustments to your UI, such as showing the action bar or
                // other navigational controls.
                handler.postDelayed({
                    window.decorView.apply {
                        // Hide both the navigation bar and the status bar.
                        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                        // a general rule, you should design your app to hide the status bar whenever you
                        // hide the navigation bar.
                        systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
                    }
                },2000)
            } else {
                // TODO: The system bars are NOT visible. Make any desired
                // adjustments to your UI, such as hiding the action bar or
                // other navigational controls.
            }
        }

    }

    override fun onBackPressed() {
        showVideoPlayingAlert()
    }


    private fun showVideoPlayingAlert() {
        val  alertDialog = AlertDialog.Builder(this@TestActivity)
        alertDialog.setTitle("Video is playing.")
        alertDialog.setMessage("Do you want to quit?")
        alertDialog.setPositiveButton("yes") {
                dialog, which -> dialog.dismiss()
            quitWatching()
        }
        alertDialog.setNegativeButton("no") {
                dialog, which -> dialog.dismiss()
        }
        alertDialog.setCancelable(true)
        alertDialog.create().show()
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.let {
            mediaPlayer -> mediaPlayer.pause()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.let {
            mediaPlayer -> mediaPlayer.stop()
            mediaPlayer.release()
        }
    }
}
