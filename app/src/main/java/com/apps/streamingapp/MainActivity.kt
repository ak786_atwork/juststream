package com.apps.streamingapp

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        playVideo()
        watch_video.setOnClickListener{
            handleInput()
        }
        url.isEnabled = false
    }

    private fun handleInput() {
        val url: String = url.text.toString()
        if (url.isNotEmpty()) {
            launchVideoActivity()
        } else {
            showToast("Please enter the url.")
        }
    }

    private fun showToast(info: String) {
        Toast.makeText(this,info, Toast.LENGTH_LONG).show()
    }

    fun pasteFromClipboard(view: View) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        var textToPaste: String
        try {
            textToPaste = clipboard.getPrimaryClip()?.getItemAt(0)?.getText().toString()
            url.setText(textToPaste)
        } catch (e: Exception) {
            return
        }

    }

    private fun launchVideoActivity() {
        val intent = Intent(this@MainActivity,TestActivity::class.java)
        intent.putExtra("url", url.text.toString())
        startActivity(intent)
        finish()
    }

    private fun playVideo() {
        val vidAddress = "https://fs157.gounlimited.to/tea5vhprp32qzxfffpyippt55gwxdrot574bj4dzq5xmbwujmluifg44tvuq/La.casa.de.papel.A.K.A.Money.Heist.S01E12.DUAL-AUDIO.SPA-ENG.720p.10bit.WEBRip.2CH.x265.HEVC-PSA.mkv.mp4"
        val vidUri = Uri.parse(vidAddress)

        video_view.setVideoURI(vidUri)
        video_view.start()
    }

    fun clearUrl(view: View) {
        url.setText("")
    }


}
